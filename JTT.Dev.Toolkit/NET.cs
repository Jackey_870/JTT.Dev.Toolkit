﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JTT.Dev.Toolkit
{
    /// <summary>
    /// 网络通讯
    /// </summary>
    namespace NET
    {
        /// <summary>
        /// 网络访问助手
        /// </summary>
        public class NETHelper
        {

            /// <summary>
            /// 获得客户端IP
            /// </summary>
            /// <param name="request"></param>
            /// <returns></returns>
            public static String GetClientIP(HttpRequestMessage _Request)
            {
                if (_Request.Properties.ContainsKey("MS_HttpContext"))
                {
                    return ((HttpContextWrapper)_Request.Properties["MS_HttpContext"]).Request.UserHostAddress;
                }
                else if (_Request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
                {
                    RemoteEndpointMessageProperty prop;
                    prop = (RemoteEndpointMessageProperty)_Request.Properties[RemoteEndpointMessageProperty.Name];
                    return prop.Address;
                }
                else
                {
                    return "";
                }
            }

            /// <summary>
            /// URLGet获取
            /// </summary>
            /// <param name="_GetURL"></param>
            /// <returns></returns>
            public static String URLGet(String _GetURL)
            {
                using (WebClient MyWebClient = new WebClient())
                {
                    MyWebClient.Encoding = Encoding.UTF8;
                    return MyWebClient.DownloadString(_GetURL);
                }
            }

            /// <summary>
            /// 文本数据Post
            /// </summary>
            /// <param name="_ServiceURL"></param>
            /// <param name="_PostDataString"></param>
            /// <param name="_PostEncoding"></param>
            /// <returns></returns>
            public static String TextMessagePost(String _ServiceURL, String _PostDataString, String _PostEncoding)
            {
                Stream MyResponseStream = default(Stream);
                //回复流
                byte[] PostBytes = Encoding.GetEncoding(_PostEncoding).GetBytes(_PostDataString);
                HttpWebRequest MyHttpWebRequest = (HttpWebRequest)WebRequest.Create(_ServiceURL);
                //填充HttpWebRequest的基本信息
                MyHttpWebRequest.ContentType = "text/plain";
                MyHttpWebRequest.Method = "POST";
                MyHttpWebRequest.Timeout = 7000; //7秒超时

                //填充要POST的内容
                MyHttpWebRequest.ContentLength = PostBytes.Length;
                Stream MyRequestStream = MyHttpWebRequest.GetRequestStream();
                MyRequestStream.Write(PostBytes, 0, PostBytes.Length);
                MyRequestStream.Close();
                //发送POST请求到服务器并读取服务器返回信息
                try
                {
                    MyResponseStream = MyHttpWebRequest.GetResponse().GetResponseStream();
                }
                catch (Exception e)
                {
                    throw e;
                }
                //读取服务器返回内容
                String TempResult = String.Empty;
                StreamReader MyResponseStreamReader = new StreamReader(MyResponseStream, Encoding.GetEncoding(_PostEncoding));
                TempResult = MyResponseStreamReader.ReadToEnd().ToString();
                MyResponseStream.Close();
                MyResponseStream.Dispose();
                return TempResult;
            }

            /// <summary>
            /// 文本数据Post
            /// </summary>
            /// <param name="_ServiceURL"></param>
            /// <param name="_PostDataString"></param>
            /// <param name="_PostEncoding"></param>
            /// <returns></returns>
            public static String TextMessagePost(String _ServiceURL, String _PostDataString)
            {
                Stream MyResponseStream = default(Stream);
                //回复流
                byte[] PostBytes = Encoding.GetEncoding("utf-8").GetBytes(_PostDataString);
                HttpWebRequest MyHttpWebRequest = (HttpWebRequest)WebRequest.Create(_ServiceURL);
                //填充HttpWebRequest的基本信息
                MyHttpWebRequest.ContentType = "text/plain";
                MyHttpWebRequest.Method = "POST";
                MyHttpWebRequest.Timeout = 7000; //7秒超时

                //填充要POST的内容
                MyHttpWebRequest.ContentLength = PostBytes.Length;
                Stream MyRequestStream = MyHttpWebRequest.GetRequestStream();
                MyRequestStream.Write(PostBytes, 0, PostBytes.Length);
                MyRequestStream.Close();
                //发送POST请求到服务器并读取服务器返回信息
                try
                {
                    MyResponseStream = MyHttpWebRequest.GetResponse().GetResponseStream();
                }
                catch (Exception e)
                {
                    throw e;
                }
                //读取服务器返回内容
                String TempResult = String.Empty;
                StreamReader MyResponseStreamReader = new StreamReader(MyResponseStream, Encoding.GetEncoding("utf-8"));
                TempResult = MyResponseStreamReader.ReadToEnd().ToString();
                MyResponseStream.Close();
                MyResponseStream.Dispose();
                return TempResult;
            }
        }
    }
}
