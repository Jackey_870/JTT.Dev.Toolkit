﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTT.Dev.Toolkit
{
    namespace Sys
    {
        /// <summary>
        /// 系统助手
        /// </summary>
        public static class SystemHelper
        {
            /// <summary>
            /// 检查是否至少Windows8.1系统
            /// </summary>
            /// <returns></returns>
            public static Boolean CheckLowestWin81()
            {
                Version MyVersion = Environment.OSVersion.Version;
                String Win81VersionString = "6.3";
                String NowVersionString = MyVersion.ToString();
                return (NowVersionString.CompareTo(Win81VersionString) >= 0);
            }
        }
    }
}
